# MLV_App_compiler

Automation tool which simplifies the compiling of a Mlv App.
[Forum_thread](https://www.magiclantern.fm/forum/index.php?topic=20025.msg186965#msg186965)

**bash menu**

![Screen Shot 2018-04-04 at 11_500px.tif](https://s15.postimg.cc/x6reo24jv/Screen_Shot_2018-05-30_at_08.56.49.png_500px.jpg)

## HOWTO ##
1. Double click MLV_App_compiler.app and follow instructions(check below how to bypass gatekeeper)

**regarding gatekeeper**
To supress gatekeeper hold ctrl button down(macOS Sierrra) while opening the application the first time. You can also change permissions from within privacy/security settings.

**MLV App is an open source project built by Ilia3101,masc,bouncyball

**License: GPL**

**Thanks to:** ML team
