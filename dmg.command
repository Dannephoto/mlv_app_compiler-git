#!/bin/bash
workingDir=`dirname "$0"`
cd "${workingDir}"

#changes size of terminal window
#tip from here http://apple.stackexchange.com/questions/33736/can-a-terminal-window-be-resized-with-a-terminal-command
#Will move terminal window to the left corner

printf '\e[8;10;95t'
printf '\e[3;410;100t'

open -a Terminal

bold="$(tput bold)"
normal="$(tput sgr0)"
red="$(tput setaf 1)"
reset="$(tput sgr0)"
green="$(tput setaf 2)"
underline="$(tput smul)"
standout="$(tput smso)"
normal="$(tput sgr0)"
black="$(tput setaf 0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
blue="$(tput setaf 4)"
magenta="$(tput setaf 5)"
cyan="$(tput setaf 6)"
white="$(tput setaf 7)"

clear
while :
do 
    cat<<EOF
    $(tput sgr0)====================			   
    ${bold}$(tput setaf 1)dmg automation script$(tput sgr0)
    --------------------

    $(tput bold)(dm) create mlv_app_compiler.dmg file$(tput sgr0)
    $(tput bold)$(tput setaf 1)(q)  exit from this menu$(tput sgr0)

Please enter your selection number below:
EOF
    read -n2
    case "$REPLY" in

   "dm")  
#let´s build the dmg file
  dir=$PWD
  cd "$dir"/
clear

#Script originally for MLVFS
#https://bitbucket.org/dmilligan/mlvfs/src/9f8191808407bb49112b9ab14c27053ae5022749/build_installer.sh?at=master&fileviewer=file-view-default
# A lot of this script came from here:
# http://stackoverflow.com/questions/96882/how-do-i-create-a-nice-looking-dmg-for-mac-os-x-using-command-line-tools
source="install_temp"
title="mlv_app_compiler"
finalDMGName="mlv_app_compiler.dmg"
size=$(echo $(du -ks | cut -d '.' -f1 | tr -d ' ')*2 | bc -l)

mkdir "${source}"
cp -R `ls | grep -v 'hg.command\|README.md\|install_temp'` "${source}"

#remove any previously existing build
rm -f "${finalDMGName}"

hdiutil create -srcfolder "${source}" -volname "${title}" -fs HFS+ -fsargs "-c c=64,a=16,e=16" -format UDRW -size ${size}k pack.temp.dmg
device=$(hdiutil attach -readwrite -noverify -noautoopen "pack.temp.dmg" | egrep '^/dev/' | sed 1q | awk '{print $1}')
sleep 2
chmod -Rf go-w /Volumes/"${title}"
sync
sync
hdiutil detach ${device}
hdiutil convert "pack.temp.dmg" -format UDZO -imagekey zlib-level=9 -o "${finalDMGName}"
rm -f pack.temp.dmg
rm -R "${source}"

#back to start
    cd "$dir"/
clear
;;

   "q")  
osascript -e 'tell application "Terminal" to close first window' & exit
;;


    "Q")  echo "case sensitive!!"   ;;
     * )  echo "invalid option"     ;;
    esac
    sleep 0.5
done






