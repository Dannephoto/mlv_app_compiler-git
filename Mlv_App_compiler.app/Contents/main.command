#GNU public license
#This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 # 
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with this program; if not, write to the
 # Free Software Foundation, Inc.,
 # 51 Franklin Street, Fifth Floor,
 # Boston, MA  02110-1301, USA.


#!/usr/bin/env bash

#start in user directory
cd ~

if ! [ -d /Library/Developer/CommandLineTools ]
then
xcode-select --install
fi

#check for dependencies
if ! [ -f /usr/local/bin/brew ]
then
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

#check for qt versions
! [ -d $(echo "/usr/local/Cellar/qt@5/5*") ] && brew install qt5 && brew upgrade qt5

#openmp
if ! [ -d /usr/local/Cellar/llvm@9 ]
then
brew install llvm@9
fi


if ! [ -d ~/MLV-App-master ]
then
git clone https://github.com/ilia3101/MLV-App.git MLV-App-master
cd ~/MLV-App-master
else
echo "updating MLV-App-master source"
cd ~/MLV-App-master
git pull
if ! [ -f $(cat /tmp/"m_a_compath2")/commits ]; then
echo "first" > $(cat /tmp/"m_a_compath2")/commits
fi
commit1=$(cat $(cat /tmp/"m_a_compath2")/commits)
commit2=$(git log --oneline -3)
if ! [ "$commit1" = "$commit2" ]; then
git log --oneline -3 > $(cat /tmp/"m_a_compath2")/commits
echo ""
echo "Recent commits:"$(tput bold)$(tput setaf 2)
sleep 0.5
git log --oneline -3 | awk 'FNR == 1'
sleep 1.5
git log --oneline -3 | awk 'FNR == 2'
sleep 1.5
git log --oneline -3 | awk 'FNR == 3'
echo ""
sleep 1
echo $(tput setaf 0)"Scroll back up for recent commits"
sleep 2
git log --oneline -10
else
echo "Recent commits:"$(tput bold)$(tput setaf 2)$(tput setaf 0)
git log --oneline -10
fi
fi

printf '\e[8;20;70t'
printf '\e[3;410;0t'
clear
while :
do 
cat<<EOF

----------------
$(tput bold)MLV App compiler$(tput sgr0)(thanks to Ilia3101,masc,bouncyball)
----------------

current branch:$(tput bold)$(tput setaf 4) $(git branch | awk '/\*/ { print $2; }')$(tput sgr0)

$(tput bold)$(tput setaf 1)(c)   compile MLV App with openmp$(tput sgr0)(macOS 11.0 and onwards)
$(tput bold)$(tput setaf 1)(b)   check out branches$(tput sgr0)
$(tput bold)$(tput setaf 1)(U)   update dependencies$(tput sgr0)(if compiling fails)
$(tput bold)$(tput setaf 1)(m)   make clean$(tput sgr0)

$(tput bold)(q)   quit this compiler$(tput sgr0)

Please enter your selection number below and press enter:
EOF
read input_variable
i=$input_variable
case "$i" in

    "co") 
clear
echo "Welcome to the wonderful world of Cocoa."
sleep 2
echo ""
echo "This version is what brought MLV app to life through Ilia3101"
sleep 2
echo ""
echo "Most features are old and might not be working!"
sleep 2
echo ""
echo "have fun!"
sleep 2
cd ~/MLV-App-master/platform/qt/
make clean 
cd ~/MLV-App-master/platform/cocoa/
make clean 
cd platform/cocoa
make app -j4
open .
echo "

scroll upwards to check terminal outcome."
    ;;


    "c") 
#automate and update to latest pixel maps from dfort repo
printf '\e[8;15;100t'
printf '\e[3;410;0t'
clear
read -p $(tput bold)"Would you like to also add updated focus pixels maps(eosm/100D)? $(tput setaf 1)Internet connection needed!$(tput bold)
(Y/N)?$(tput sgr0)
" choice
case "$choice" in 
  y|Y ) 
#grab scripts for MLV_App_compiler
mapyes=$(echo mapyes)
esac

echo "let´s clean repo first(make clean)!"
sleep 1
cd ~/MLV-App-master/platform/qt/
make clean 
cd ~/MLV-App-master/platform/cocoa/
make clean 
cd ~/MLV-App-master/platform
rm -r Mlv_app_master
mkdir -p Mlv_app_master
cd Mlv_app_master


#If running MLVApp.pro file uncomment this
#$(ls -d /usr/local/Cellar/qt@5/5* | head -1 | tr -d ':')/bin/qmake ~/MLV-App-master/platform/qt/MLVApp.pro && /usr/bin/make -j4 && \
#$(ls -d /usr/local/Cellar/qt@5/5* | head -1 | tr -d ':')/bin/macdeployqt ~/MLV-App-master/platform/Mlv_app_master/MLV\ App.app && \
#make clean


$(ls -d /usr/local/Cellar/qt@5/5* | head -1 | tr -d ':')/bin/qmake ~/MLV-App-master/platform/qt/MLVApp.pro \
QMAKE_CC=/usr/local/opt/llvm@9/bin/clang \
QMAKE_CXX=/usr/local/opt/llvm@9/bin/clang++ \
QMAKE_LINK=/usr/local/opt/llvm@9/bin/clang++ \
QMAKE_CFLAGS+=-fopenmp \
QMAKE_CFLAGS+=-ftree-vectorize \
QMAKE_CXXFLAGS+=-fopenmp \
QMAKE_CXXFLAGS+=-std=c++11 \
QMAKE_CXXFLAGS+=-ftree-vectorize \
INCLUDEPATH+=-I/usr/local/opt/llvm@9/include \
LIBS+=-L/usr/local/opt/llvm@9/lib LIBS+=-lomp \
QMAKE_MACOSX_DEPLOYMENT_TARGET=10.8 -spec macx-clang \
CONFIG+=release && \
/usr/bin/make -j4 && \
$(ls -d /usr/local/Cellar/qt@5/5* | head -1 | tr -d ':')/bin/macdeployqt ~/MLV-App-master/platform/Mlv_app_master/MLV\ App.app && \
make clean


if [ "$mapyes" = "mapyes" ]
then
echo "###############"
echo "##############################"
echo "############################################################"
echo $(tput bold)$(tput setaf 1)"Downloading and installing pixel map files, please wait"$(tput sgr0)
echo ""
echo ""
mapyes=


curl -L https://bitbucket.org/Dannephoto/mlv_app_compiler-git/downloads/focus_pixel_maps.zip -o ~/MLV-App-master/platform/Mlv_app_master/focus_pixel_maps.zip
unzip -qq -o ~/MLV-App-master/platform/Mlv_app_master/focus_pixel_maps.zip -d ~/MLV-App-master/platform/Mlv_app_master/
rm ~/MLV-App-master/platform/Mlv_app_master/focus_pixel_maps.zip 
mv ~/MLV-App-master/platform/Mlv_app_master/focus_pixel_maps/*.fpm ~/MLV-App-master/platform/Mlv_app_master/MLV\ App.app/Contents/MacOS
rm -r ~/MLV-App-master/platform/Mlv_app_master/focus_pixel_maps

echo ""
echo ""
echo $(tput bold)$(tput setaf 1)"Pixel map files should now be installed"$(tput sgr0)
echo ""
echo ""

echo "############################################################" 
echo "##############################"
echo "###############"
fi && \
open ~/MLV-App-master/platform/Mlv_app_master/
echo "

scroll upwards to check terminal outcome."
    ;;

    "b")
printf '\e[8;18;70t'
printf '\e[3;410;0t'
while :
do 
branches=$(git branch -a | awk '{printf("%01d %s\n", NR, $0)}' | cut -d '/' -f3 | cut -d ' ' -f3)
/usr/bin/osascript -e 'tell application "System Events" to tell process "Terminal" to keystroke "k" using command down'
clear
cat<<EOF

current branch:$(tput bold)$(tput setaf 4) $(git branch | awk '/\*/ { print $2; }')$(tput sgr0)

$(tput bold)$(printf "%s\n" $branches | awk '{printf("%01d %s\n", NR, $0)}' | awk '{$1=$1}1' OFS="\t")$(tput sgr0)

$(tput bold)$(tput setaf 1)(m)   main menu$(tput sgr0)
$(tput bold)(q)   quit this compiler$(tput sgr0)

Please enter branch number and press enter:
EOF
read input_variable
i=$input_variable
case "$i" in

    "q") 
osascript -e 'tell application "Terminal" to close first window' & exit
    ;;

    "m") 
. "$(cat /tmp/m_a_compath2)"/main.command
    ;;

    "$i") 
/usr/bin/osascript -e 'tell application "System Events" to tell process "Terminal" to keystroke "k" using command down'
git checkout $(printf "%s\n" $branches | awk 'FNR == "'$i'"')
. "$(cat /tmp/m_a_compath2)"/main.command
    ;;

    esac
done

    ;;

    "U") 
clear
echo "Updating!"
#start in user directory
cd ~
xcode-select --install
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
#check for qt versions
brew install qt5
brew upgrade qt5
brew install llvm@9
brew upgrade llvm@9
echo "Rerun the compiler when you are done!"
sleep 2
    ;;

    "m") 
echo "cleaning!"
sleep 1
cd ~/MLV-App-master/platform/qt/
make clean 
cd ~/MLV-App-master/platform/cocoa/
make clean 
cd ~/MLV-App-master/
clear
    ;;

    "q") 
osascript -e 'tell application "Terminal" to close first window' & exit
    ;;

    esac
done
    ;;




